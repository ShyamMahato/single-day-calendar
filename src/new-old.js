import { React, Component } from "react";
import "./App.css";

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      timeRange: [
        "9 AM",
        "10 AM",
        "11 AM",
        "12 AM",
        "01 PM",
        "02 PM",
        "03 PM",
        "04 PM",
        "05 PM",
        "06 PM",
        "07 PM",
        "08 PM",
        "09 PM",
      ],

      timeIndex: {
        900: 1,
        1000: 2,
        1100: 3,
        1200: 4,
        1300: 5,
        1400: 6,
        1500: 7,
        1600: 8,
        1700: 9,
        1800: 10,
        1900: 11,
        2000: 12,
        2100: 13,
      },

      newEventList: [],
    };
  }

  componentDidMount() {
    fetch(
      "https://recruiter-static-content.s3.ap-south-1.amazonaws.com/json_responses_for_tests/test.json"
    )
      .then((data) => {
        return data.json();
      })
      .then((result) => {
        this.setState(
          {
            calendarData: result,
          },
          () => {
            const eventTime = {
                900: [],
                1000: [],
                1100: [],
                1200: [],
                1300: [],
                1400: [],
                1500: [],
                1600: [],
                1700: [],
                1800: [],
                1900: [],
                2000: [],
                2100: [],
              },
              newEventList = this.state.calendarData.reduce((acc, event) => {
                if (event.start <= "1000") {
                  acc["900"].push(event);
                } else if (event.start <= "1100") {
                  acc["1000"].push(event);
                } else if (event.start <= "1200") {
                  acc["1100"].push(event);
                } else if (event.start <= "1300") {
                  acc["1200"].push(event);
                } else if (event.start <= "1400") {
                  acc["1300"].push(event);
                } else if (event.start <= "1500") {
                  acc["1400"].push(event);
                } else if (event.start <= "1600") {
                  acc["1500"].push(event);
                } else if (event.start <= "1700") {
                  acc["1600"].push(event);
                } else if (event.start <= "1800") {
                  acc["1700"].push(event);
                } else if (event.start <= "1900") {
                  acc["1800"].push(event);
                } else if (event.start <= "2000") {
                  acc["1900"].push(event);
                } else if (event.start <= "2100") {
                  acc["2000"].push(event);
                }

                return acc;
              }, eventTime);

            this.setState({
              newEventList: newEventList,
            });
          }
        );
      })
      .catch((error) => {
        console.log(error);
      });
  }

  RowEnd = (start, end) => {
    return Math.ceil((end - Number(start.slice(0, 2) + "00")) / 100);
  };

  columnEnd = () => {};

  timeFormat = (time) => {
    return time.slice(0, 2) + ":" + time.slice(2);
  };

  parseTime(time) {
    const hour = time.slice(0, 2);
    const minute = time.slice(2);
    return parseInt(hour) * 60 + parseInt(minute);
  }

  render() {
    return (
      <div className="calendar-container">
        <div className="calendar-header">
          <p>Monday, 18th Jan</p>
          <hr />
        </div>
        <div className="calendar-body">
          <div className="time-container">
            <div className="time-sub-container">
              {this.state.timeRange.map((time, index) => {
                return (
                  <div key={index} className="time">
                    {time}
                  </div>
                );
              })}
            </div>

            <div className="sub-container">
              {Object.keys(this.state.newEventList).map((time, index) => {
                if (this.state.newEventList[time].length === 0) {
                  return (
                    <div
                      key={index}
                      className="one"
                      style={{
                        gridRowStart: `${index + 1}`,
                        gridRowEnd: `${index + 1}`,
                      }}
                    >
                      &nbsp;
                    </div>
                  );
                } else {
                  return this.state.newEventList[time].map(
                    (eventObj, index) => {
                      return (
                        <div
                          key={index}
                          className="two"
                          style={{
                            marginTop: `${Number(eventObj.start.slice(2))}px`,
                            height: `${
                              this.parseTime(eventObj.end) -
                              this.parseTime(eventObj.start)
                            }px`,
                            gridRowStart: `${this.state.timeIndex[time]}`,
                            gridRowEnd: `${
                              this.state.timeIndex[time] +
                              this.RowEnd(eventObj.start, eventObj.end)
                            }`,
                            // gridColumnEnd: `${this.columnEnd()}`,
                          }}
                        >
                          {`Event ${eventObj.title} ${this.timeFormat(
                            eventObj.start
                          )} ${this.timeFormat(eventObj.end)}`}
                        </div>
                      );
                    }
                  );
                }
              })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
