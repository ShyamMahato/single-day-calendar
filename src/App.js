import { React, Component } from "react";
import "./App.css";

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      calendarData: [],
      timeRange: [
        "9 AM",
        "10 AM",
        "11 AM",
        "12 AM",
        "01 PM",
        "02 PM",
        "03 PM",
        "04 PM",
        "05 PM",
        "06 PM",
        "07 PM",
        "08 PM",
      ],

      timeIndex: {
        900: 1,
        1000: 2,
        1100: 3,
        1200: 4,
        1300: 5,
        1400: 6,
        1500: 7,
        1600: 8,
        1700: 9,
        1800: 10,
        1900: 11,
        2000: 12,
      },

      newEventList: [],
      maxEventInARow: 0,
    };
  }

  componentDidMount() {
    fetch(
      "https://recruiter-static-content.s3.ap-south-1.amazonaws.com/json_responses_for_tests/test.json"
    )
      .then((data) => {
        return data.json();
      })
      .then((result) => {
        this.setState(
          {
            calendarData: result,
          },
          () => {
            const eventTime = {
              900: [],
              1000: [],
              1100: [],
              1200: [],
              1300: [],
              1400: [],
              1500: [],
              1600: [],
              1700: [],
              1800: [],
              1900: [],
              2000: [],
            };

            const newEventList = this.state.calendarData.reduce(
              (acc, event) => {
                if (event.start <= "1000") {
                  acc["900"].push(event);
                } else if (event.start <= "1100") {
                  acc["1000"].push(event);
                } else if (event.start <= "1200") {
                  acc["1100"].push(event);
                } else if (event.start <= "1300") {
                  acc["1200"].push(event);
                } else if (event.start <= "1400") {
                  acc["1300"].push(event);
                } else if (event.start <= "1500") {
                  acc["1400"].push(event);
                } else if (event.start <= "1600") {
                  acc["1500"].push(event);
                } else if (event.start <= "1700") {
                  acc["1600"].push(event);
                } else if (event.start <= "1800") {
                  acc["1700"].push(event);
                } else if (event.start <= "1900") {
                  acc["1800"].push(event);
                } else if (event.start <= "2000") {
                  acc["1900"].push(event);
                }

                return acc;
              },
              eventTime
            );

            const eventSorted = {};

            Object.keys(newEventList).forEach((time) => {
              eventSorted[time] = newEventList[time].sort((a, b) => {
                if (a.start < b.start) {
                  return -1;
                }
                if (a.start > b.start) {
                  return 1;
                }
                return 0;
              });
            });

            this.setState(
              {
                newEventList: eventSorted,
              },
              () => {
                const maxEventInARow = this.getMaxEventInRow();

                this.setState({
                  maxEventInARow: maxEventInARow,
                });
              }
            );
          }
        );
      })
      .catch((error) => {
        console.log(error);
      });
  }

  getMaxEventInRow = () => {
    let maxEventInARow = 0;

    Object.keys(this.state.newEventList).forEach((events) => {
      if (this.state.newEventList[events].length > maxEventInARow) {
        maxEventInARow = this.state.newEventList[events].length;
      }
    });

    return maxEventInARow;
  };

  timeFormat = (time) => {
    return time.slice(0, 2) + ":" + time.slice(2);
  };

  parseTime(time) {
    const hour = time.slice(0, 2);
    const minute = time.slice(2);
    return parseInt(hour) * 60 + parseInt(minute);
  }

  previousEvent = (currEvent, index) => {
    return Object.keys(this.state.newEventList).reduce((acc, ev) => {
      this.state.newEventList[ev].forEach((obj, newInd) => {
        if (
          obj.start < currEvent.start &&
          obj.end > currEvent.start &&
          index > this.state.timeIndex[ev] + newInd
        ) {
          acc++;
        }
      });

      return acc;
    }, 0);
  };

  nextEvent = (currEvent, index) => {
    return Object.keys(this.state.newEventList).reduce((acc, ev) => {
      this.state.newEventList[ev].forEach((obj, newInd) => {
        if (
          currEvent.end > obj.start &&
          obj.title !== currEvent.title &&
          index <= this.state.timeIndex[ev] + newInd
        ) {
          acc++;
        }
      });

      return acc;
    }, 0);
  };

  getWidth = (prevEventCount, nextEventCount) => {
    if (prevEventCount + nextEventCount === this.state.maxEventInARow)
      return 600 / this.state.maxEventInARow + "px";

    return 600 / (nextEventCount + prevEventCount + 1) + "px";
  };

  getMarginLeft = (prevEventCount, nextEventCount) => {
    if (prevEventCount === 0) return 0;
    if (prevEventCount + nextEventCount === this.state.maxEventInARow) return 0;

    if (prevEventCount + nextEventCount + 1 === this.state.maxEventInARow)
      return 0;

    return 600 / (prevEventCount + nextEventCount + 1) + "px";
  };

  render() {
    return (
      <div className="calendar-container">
        <div className="calendar-body">
          <div className="time-container">
            {this.state.timeRange.map((time, index) => {
              return (
                <div key={index} className="time">
                  {time}
                </div>
              );
            })}
          </div>
          <div className="sub-container">
            {Object.keys(this.state.newEventList).map((time, eventIndex) => {
              return (
                <div className="row" key={eventIndex}>
                  {this.state.newEventList[time].map((eventObj, index) => {
                    const checkPreviousEvent = this.previousEvent(
                      eventObj,
                      this.state.timeIndex[time] + index
                    );
                    const checkNextEvent = this.nextEvent(
                      eventObj,
                      this.state.timeIndex[time] + index
                    );

                    return (
                      <div
                        key={index}
                        className="event"
                        style={{
                          width: this.getWidth(
                            checkPreviousEvent,
                            checkNextEvent
                          ),
                          marginLeft: this.getMarginLeft(
                            checkPreviousEvent,
                            checkNextEvent
                          ),
                          marginTop: `${Number(eventObj.start.slice(2))}px`,
                          height: `${
                            this.parseTime(eventObj.end) -
                            this.parseTime(eventObj.start)
                          }px`,
                        }}
                      >
                        {`Event ${eventObj.title} ${this.timeFormat(
                          eventObj.start
                        )} ${this.timeFormat(eventObj.end)}`}
                      </div>
                    );
                  })}
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}
